<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth'], function () {

    Route::get('v1/students','Api\v1\StudentController@getAll');
    
    Route::post('v1/students','Api\v1\StudentController@save');
    
    Route::patch('v1/students/{id}','Api\v1\StudentController@update');
    
    Route::delete('v1/students/{id}','Api\v1\StudentController@delete');
    
    Route::get('v1/students/{id}','Api\v1\StudentController@get');    
});


Route::post('v1/users/login', 'Api\v1\UserController@login');

Route::post('v1/users/register', 'Api\v1\UserController@register');

Route::get('v1/users/forgetPassword', 'Api\v1\UserController@forgetPassword');

Route::post('v1/users/changePassword', 'Api\v1\UserController@changePassword')->middleware('tokenAuth');
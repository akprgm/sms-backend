<?php

namespace App\Models;

class InterestStudent extends Model
{
    protected $table = 'interest_student';
    protected $fillable = ['student_id','interest_id'];
}

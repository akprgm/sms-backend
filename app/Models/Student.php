<?php

namespace App\Models;

class Student extends Model
{
    protected $fillable = ['name','user_id','address','gender','year_of_passing'];
    
    public function roles()
    {
        return $this->belongsToMany('App\Models\Interest');
    }
}

<?php 

namespace App\Repositories;

use App\Models\Student;
use App\Models\Interest;
use App\Models\InterestStudent;
use App\Services\Service;

class StudentRepository extends Repository
{
    public function getAll ()
    {
        $student = new Student();

        try {

            $response = $student->paginate(15);

        } catch (\Illuminate\Database\QueryException $e) {

            return $this->serverErrorResponse();

        } catch (\Exception $e) {

            return $this->serverErrorResponse();
            
        }
        return $this->successDataResponse($response); 
        /*if (sizeof($response)) {
            return $this->successDataResponse($response);        
        } else {    
            return $this->emptyResponse();
        }*/
    }

    public function save ($inputs)
    {    
        try {  
            $response = Student::create($inputs);

            $studentId  = $response['id'];
        
            $interest = $inputs['interest'];

            $this->saveInterest($interest, $studentId);

        } catch (\Illuminate\Database\QueryException $e) {
            
            return $this->conflictResponse("student already exists");
        
        } catch (\Exception $e) {
            
            return $this->serverErrorResponse();

        }
        
        return $this->successResponse();
    }

    public function get ($id)
    {
        try {

            $response = Student::find($id);

            $interest = InterestStudent::where('student_id',$id)->select('interest_id')->get();

            $newInterest = array();

            foreach($interest as $value) {

                $actualinterest = Interest::where('id',$value->interest_id)->first();

                array_push($newInterest,$actualinterest['interest']);
            }
            $response->interest = $newInterest;

        } catch (\Illuminate\Database\QueryException $e) {
        
            return $this->notFoundResponse();
        
        } catch (\Exception $e) {
    
            return $this->serverErrorResponse();

        }

        return $this->successDataResponse($response);
    }

    public function update ($inputs)
    {           
        try {
            array_pull($inputs,'id');

            $interest = array_pull($inputs, 'interest');
            
            $studentId = array_pull($inputs , 'student_id');

            $userId = array_pull($inputs, 'user_id');

            Student::where("id",$studentId)->update($inputs);        
            
            $this->updateInterest($interest, $studentId);

        } catch (\Illuminate\Database\QueryException $e) {
            
            return $this->conflictResponse();
        
        } catch (\Exception $e) {
            dd($e);
            return $this->serverErrorResponse();

        }

        return $this->successResponse();
    }

    public function delete ($id)
    {
        try {
            $response = InterestStudent::where('student_id',$id)->delete();
            
            $response = Student::destroy($id);

        } catch (\Illuminate\Database\QueryException $e) {
            return $this->notFoundResponse();
        
        } catch (\Exception $e) {

            return $this->serverErrorResponse();

        }

        return $this->emptyResponse();
    }

    public function saveInterest($interest, $studentId)
    {
        foreach ($interest as $value) {

            $checkInterest = Interest::where('interest',$value)->first();
            
            if (sizeof($checkInterest)) {

                $checkExistInterest = InterestStudent::where('interest_id',$checkInterest->id)->where('student_id',$studentId)->get();
                
                if(!sizeof($checkExistInterest)){
                    InterestStudent::create(["student_id"=>$studentId, "interest_id"=>$checkInterest->id]);                
                }            
            } else {
                $newinterest = Interest::create(["interest"=>$value]);

                InterestStudent::create(["student_id"=>$studentId, "interest_id"=>$newinterest['id']]);    
            }
        }
        return ;
    }

    public function updateInterest($interest,$studentId)
    {
        $newInterest = array();

        foreach ($interest as $value) {

            $value = strtolower($value);            

            $checkInterest = Interest::where('interest',$value)->first();
            
            if (!sizeof($checkInterest)) {

                $createdInterest = Interest::create(["interest"=>$value]);
                
                array_push($newInterest,$createdInterest['id']);

            } else {

                array_push($newInterest,$checkInterest->id);

            }
        }
    
        $student = Student::find($studentId);

        $student->roles()->sync($newInterest);
    }
}

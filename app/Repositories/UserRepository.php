<?php 

namespace App\Repositories;

use Illuminate\Support\Facades\Config;
use App\Models\User;
use JWTAuth;
use JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException; 
use Illuminate\Support\Facades\Hash;
use App\Services\Service;

class UserRepository extends Repository
{
    public function login ($credentials)
    { 
        $user = User::where('email',$credentials['email'])->where('status',1)->get();

        if (count($user)) {
            try {
                if (! $token = JWTAuth::attempt($credentials)) {

                    return $this->badResponse("Invalid Email or Password");
                }
            } catch (JWTException $e) {

                return $this->serverErrorResponse();

            } catch (\Exception $e) {
            
            }

            return $this->SuccessDataResponse($token);
        } else {
            return $this->verifyAccountResponse();
        }
    }

    public function register ($inputs)
    {
        $inputs['password'] = Hash::make($inputs['password']);
        
        try {
            
            $user = User::create($inputs);

        } catch (\Illuminate\Database\QueryException $e) {

            return $this->userExistResponse();

        } catch (\Exception $e) {
            
            return $this->serverErrorResponse();
        
        }

        $token = JWTAuth::fromUser($user);

        Config::set('jwt.ttl','');

        $payload = JWTFactory::make(["email"=>$inputs['email']]);

        $token = JWTAuth::encode($payload);

        $verify_url = \App::make('url')->to('/').'/verifyAccount?token='.(string)$token;

        $this->sendMail($inputs['email'],$verify_url);

        return $this->successDataResponse("Please check your Mailbox to verify your account");
    }

    public function forgetPassword ($inputs)
    {   
        $email = $inputs['email'];
        
        try {
            
            $user = User::where('email',$email)->get();

        } catch (\Illuminate\Database\QueryException $e) {

            return $this->NotFoundResponse();

        }
        
        if (!count($user)) {
            
            return $this->NotFoundResponse("email not registered");
        
        }

        Config::set('jwt.ttl','15');
        
        $payload = JWTFactory::make($inputs);

        $token = JWTAuth::encode($payload);

        $pwd_url = 'http://demo.app.squareboat.com/changePassword?token='.(string)$token;
      
        $this->sendMail($email,$pwd_url);

        return $this->successDataResponse("Please check your email for changing the password of your account");
    }

    public function changePassword ($inputs)
    {   

        $password = Hash::make($inputs['password']);
                
        try {

            $updateUser = User::where('email',$inputs['email'])->update(['password'=>$password]);
            
        } catch (\Illuminate\Database\QueryException $e) {

            return $this->serverErrorResponse();
        
        } catch (\Exception $e) {
            
            return $this->serverErrorResponse();

        }
        
        if (!$updateUser) {

            return $this->notFoundResponse();
        }

        return $this->successResponse();
    }

    public function verifyAccount ($inputs)
    {
        try {

            $updateUser = User::where('email',$inputs['email'])->update(['status'=>1]);
            
        } catch (\Illuminate\Database\QueryException $e) {

            return $this->serverErrorResponse();
        
        } catch (\Exception $e) {
            
            return $this->serverErrorResponse();

        }

        if (!$updateUser) {

            return $this->notFoundResponse();
        }

        return redirect('http://demo.app.squareboat.com');

    }
    
}
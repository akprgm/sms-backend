<?php 

namespace App\Repositories;

use Mail;

class Repository
{

	public function successResponse ()
    {
        return response()->json(array("data"=>"Request proccessed successfully"), 200);
    }

    /**
	 * returns the response object in case successfull request along with data
	 *
	 * @param $response
	 * @return response object
	 */
    public function successDataResponse ($response)
    {
        return response()->json(array("data"=>$response), 200);  
    }

    /**
	 * returns the response object in case of invalid username and password
	 *
	 * @param 
	 * @return response object
	 */
    public function badResponse ($msg="Bad Request")
    {
        return response()->json(array("data"=>$msg), 400);        
    }
    
    /**
	 * returns the response object in case of Unauthorised request
	 *
	 * @param 
	 * @return response object
	 */
    public function unauthorisedResponse ()
    {
        return response()->json(array("data"=>"Unauthorised Request"), 401);        
    }

    /**
	 * returns the response object in case of server error
	 *
	 * @param 
	 * @return response object
	 */
    public function serverErrorResponse ()
    {   
        return response()->json(array("data"=>"Internal Server Error, Please try later"), 500);        
    }

    /**
	 * returns the response object in case of new resource created by request
	 *
	 * @param 
	 * @return response object
	 */
    public function createResponse ()
    {
        return response()->json(array("data"=>"Request proccessed successfully"), 201);            
    }

    /**
	 * returns the response object in case of conflict in creating new resource
	 *
	 * @param 
	 * @return response object
	 */
    public function conflictResponse ($msg="Conflict occur in proccessing this Request ")
    {
        return response()->json(array("data"=>$msg), 422);
                
    }

    public function emptyResponse () 
    {
        return response()->json(array("data"=>"no content "), 204);
        
    }

    /**
	 * returns the response object in case of resource not found
	 *
	 * @param 
	 * @return response object
	 */
    public function notFoundResponse ($msg="invalid request")
    {
        return response()->json(array("data"=>$msg), 404 );               
    }

    public function verifyAccountResponse ()
    {
        return response()->json(array("data"=>"verify your account"), 422 );               
    }

    public function userExistResponse ()
    {
        return response()->json(array("data"=>"User Already Exist"), 422 ); 
    }

    public function sendMail($email, $text)
    {
        Mail::send('mail', ['title' => 'Demo', 'content' => $text], function ($message) use ($email)
        {
            $message->to($email);

        });

        return ;
    }
}

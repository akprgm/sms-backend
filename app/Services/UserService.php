<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\Validations\UserValidator;

class UserService extends Service
{
    /**
	 * The user repository instance.
	 *
	 * @var \App\Repositories\UserRepository
	 */
    public $userRepository;
    public $userValidator;

    /**
	 * Create a new services instance.
	 *
	 * @param  \App\Repositories\UserRepository $user
	 * @return void
	 */
    function __construct (UserRepository $userRepository, UserValidator $userValidator)
    {
        $this->userRepository = $userRepository;
        $this->validator = $userValidator;
    }

    /**
	 * login user.
	 *
	 * @param email and password
	 * @return token
	 */
    function login($inputs)
    {
        $response = $this->validator->validate('login',$inputs);

        if (!$response) {
            $response = $this->userRepository->login($inputs);        
        }

        return $response;
    }

    /**
	 * register new user.
	 *
	 * @param email and password
	 * @return token
	 */
    function register($inputs)
    {
        $response = $this->validator->validate('register',$inputs);

        if (!$response) {
            $response = $this->userRepository->register($inputs);
        }
        return $response;
    }

    /**
	 * forget password.
	 *
	 * @param email
	 * @return url to reset password
	 */
    function forgetPassword ($inputs)
    {
        $response = $this->validator->validate('forgetPwd',$inputs);

        if (!$response) {
            $response = $this->userRepository->forgetPassword($inputs);
        }
        
        return $response;
    }

    /**
	 * change password.
	 *
	 * @param email and new password
	 * @return success response
	 */
    function changePassword ($inputs)
    {
        $response = $this->validator->validate('changePwd',$inputs);

        if (!$response) {
            $response = $this->userRepository->changePassword($inputs);
        }
        return $response;
    }

    /**
	 * verify Account.
	 *
	 * @param void
	 * @return success response
	 */
     function verifyAccount ($inputs)
     {
         $response = $this->userRepository->verifyAccount($inputs);

         return $response;
     }

    
}
<?php

namespace App\Services;

use App\Repositories\StudentRepository;
use App\Validations\StudentValidator;

class StudentService extends Service
{
	/**
	 * The student repository instance.
	 *
	 * @var \App\Repositories\StudentRepository
	 */
	public $studentRepository;

	public $studentValidator;
	/**
	 * Create a new services instance.
	 *
	 * @param  \App\Repositories\StudentRepository $student
	 * @return void
	 */
	function __construct (StudentRepository $studentRepository, Studentvalidator $validator)
	{
		$this->studentRepository = $studentRepository;

		$this->validator = $validator;
	}

    /**
	 * gets all students in database with pagination.
	 *
	 * @param
	 * @return students response
	 */
    public function getAll ()
    {
        $response = $this->studentRepository->getAll();

        return $response;
    }
    
    /**
	 * gets details of student.
	 *
	 * @param  student id
	 * @return student response
	 */
    public function get ($id)
    {
		$response = $this->validator->validate('show',["id"=>$id]);
		
		if (!$response) {
        	$response = $this->studentRepository->get($id);
		}

        return $response;
    }

    /**
	 * updates student in database.
	 *
	 * @param student id, student details 
	 * @return student response
	 */
    public function update ($inputs, $id)
    {
		$inputs['student_id'] = $id;

		$inputs['user_id'] = $inputs['id'];

		$response = $this->validator->validate('update',$inputs);
		
		if (!$response) {
	        $response = $this->studentRepository->update($inputs, $id);		
		}

        return $response;
    }

    /**
	 * save new student in database.
	 *
	 * @param student details 
	 * @return student response
	 */
	public function save ($inputs)
    {
		$inputs['user_id'] = $inputs['id'];
	
		$response = $this->validator->validate('create', $inputs);
        
		if (!$response) {
			$response = $this->studentRepository->save($inputs);		
		}

        return $response;
    }

    /**
	 * deletes student in database.
	 *
	 * @param student id 
	 * @return student response
	 */
    public function delete ($id)
    {
		$response = $this->validator->validate('delete',["id"=>$id]);
		
        $response = $this->studentRepository->delete($id);

        return $response;
    }
}

<?php

namespace App\Validations;

use Validator;
use Illuminate\Http\Request;

class StudentValidator
{
	/**
	 * validation rules
	 *
	 * @return array
	 */
	public function validate($type, $data)
	{
		//print_r($data);
		switch($type)
		{
			case 'create':
				$validator = Validator::make($data, [
					'user_id' => 'required|integer',
					'name' => "required",
					'gender' => 'required|String',
					'year_of_passing' => 'required|Integer',
					'address' => 'required',
					'interest' => 'Array'		
				]);
				if ($validator->fails()) {
					
					return $this->validationMessage();
				}
			break;

            case 'update':
                $validator = Validator::make($data, [
					'student_id' => 'required|integer',
					'user_id' => 'required|integer',
					'name' => "required",
					'gender' => 'required|String',
					'year_of_passing' => 'required|Integer',
					'address' => 'required',
					'interest' => 'Array',
				]);
				if ($validator->fails()) {
					return $this->validationMessage();
				}
            break;
            
			case 'delete':
                $validator = Validator::make($data, [
                    'id' => "required|integer"
                ]);
				if ($validator->fails()) {
					return $this->validationMessage();
				}
            break;

            case 'show':
				$validator = Validator::make($data, [
                    'id' => "required|integer"
                ]);
				if ($validator->fails()) {
					return $this->validationMessage();
				}
            break;
		}
		return ;
	}

	protected function validationMessage(){
		return response()->json(array("data"=>"Validation failed"), 422);
	}

}

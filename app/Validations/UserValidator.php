<?php

namespace App\Validations;

use Validator;
use Illuminate\Http\Request;

class UserValidator
{
	/**
	 * validation rules
	 *
	 * @return array
	 */
	public function validate($type, $data)
	{
		switch($type)
		{
			case 'login':
				$validator = Validator::make($data, [
					'email' => 'required|',
					'password' => "required|min:8",		
				]);
				if ($validator->fails()) {
					return $this->validationMessage();
				}
			break;

            case 'register':
                $validator = Validator::make($data, [
					'email' => 'required',
					'password' => 'required|min:8',
				]);
				if ($validator->fails()) {
					return $this->validationMessage();
				}
            break;

			case 'forgetPwd':
                $validator = Validator::make($data, [
					'email' => 'required',
				]);
				if ($validator->fails()) {
					return $this->validationMessage();
				}
            break;

			case 'changePwd':
                $validator = Validator::make($data, [
					'email' => 'required',
					'password' => 'required|min:8',
				]);
				if ($validator->fails()) {
					return $this->validationMessage();
				}
            break;
		}
		return ;
	}

	protected function validationMessage(){
		return response()->json(array("data"=>"Invalid input Credentials"), 422);
	}

}

<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Log;
class TokenAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    
        try {
           $response = JWTAuth::parseToken()->getPayload();

        } catch (TokenExpiredException $e) {

            return response()->json(array("data"=>'Token expired'), $e->getStatusCode());

        } catch (TokenInvalidException $e) {

            return response()->json(array("data"=>'Token invalid'), $e->getStatusCode());

        } catch (JWTException $e) {

            return response()->json(array("data"=>'Token absent'), $e->getStatusCode());

        }
             
        $request->request->add(["email"=>$response['email']]);
        
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\JWTException; 

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(array("data"=>'User not found'), 404);
            }

        } catch (TokenExpiredException $e) {

            return response()->json(array("data"=>'Token expired'), $e->getStatusCode());

        } catch (TokenInvalidException $e) {

            return response()->json(array("data"=>'Token invalid'), $e->getStatusCode());

        } catch (JWTException $e) {

            return response()->json(array("data"=>'Token absent'), $e->getStatusCode());

        }

        if($user->id){
            $request->request->add(["id"=>$user->id]);        
        }

        return $next($request);
    }
}

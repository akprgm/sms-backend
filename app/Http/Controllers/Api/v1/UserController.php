<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Services\UserService;

class UserController extends Controller
{
     protected $userService;

     function __construct (UserService $userService)
     {
         $this->userService = $userService;
     }  

     /**
     * login user.
     *
     * @param  string email & password
     * @return \Illuminate\Http\Response
     */
     public function login (Request $request)
     {
        $credentials = $request->only('email', 'password');
        
        $response = $this->userService->login($credentials);

        return $response;
     }

     /**
     * registering user in database.
     *
     * @param  email & password
     * @return \Illuminate\Http\Response
     */
     public function register (Request $request)
     {
         $inputs = $request->all();

         $response = $this->userService->register($inputs);

         return $response;
     }

     /**
     * forget password .
     *
     * @param  email
     * @return \Illuminate\Http\Response
     */
     public function forgetPassword (Request $request)
     {
         $inputs = $request->all();
    
         $response = $this->userService->forgetPassword($inputs);

         return $response;
     }

     /**
     * change password for user.
     *
     * @param  email and new password
     * @return \Illuminate\Http\Response
     */
     public function changePassword (Request $request)
     {
         $inputs = $request->all();

         $response = $this->userService->changePassword($inputs);

         return $response;
     }

     /**
     * verify account for user.
     *
     * @param void
     * @return \Illuminate\Http\Response
     */
     public function verifyAccount (Request $request)
     {
         $inputs = $request->all();

         $response = $this->userService->verifyAccount($inputs);

         return $response;
     }

}
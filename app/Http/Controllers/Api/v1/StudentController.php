<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Services\StudentService;

class StudentController extends Controller
{
    /**
	 * The student service instance.
	 *
	 * @var StudentService
	 */
	protected $studentService;

	/**
	 * Create a new controller instance.
	 *
	 * @param  PatientService $patients
	 * @return void
	 */
	function __construct (StudentService $studentService)
	{
		$this->studentService = $studentService;
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll ()
    {
        $response = $this->studentService->getAll();
        
        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save ()
    {
        $inputs = request()->all();

        $response = $this->studentService->save($inputs);

        return $response; 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function get ($id)
    {
        $response = $this->studentService->get($id);

        return $response;   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update (Request $request, $id)
    {
        $inputs = request()->all();

        $response = $this->studentService->update($inputs, $id);

        return $response; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete ($id)
    { 
        $response = $this->studentService->delete($id);

        return $response;        
    }
}

<!doctype html>
<html>
    <head>
        <title>SMS</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
    </head>
</html>
    <body>
        <header role="banner" class="navbar navbar-fixed-top navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <button data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" type="button" class="navbar-toggle pull-left"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div class="navbar-inverse side-collapse in">
                    <nav role="navigation" class="navbar-collapse">
                        <ul class="nav navbar-nav">
                        <li><a href="/sms">Home</a></li>
                        <li><a href="../index.php/add">Add New Student</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <div class="container" style="width:75%;margin-top:2%">
            <form id="addStudentForm" class="form-horizontal" role="form">
                <h2 style="text-align:center">Registering New Student</h2>
                <div class="form-group">
                    <label for="firstName" class="col-sm-3 control-label">Full Name</label>
                    <div class="col-sm-9">
                        <input type="text" id="name" placeholder="Full Name" required class="form-control" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" id="email" placeholder="Email" required class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="country" class="col-sm-3 control-label">Year of Passing</label>
                    <div class="col-sm-9">
                        <select id="yop" class="form-control" required>
                            <option>2011</option>
                            <option>2012</option>
                            <option>2013</option>
                            <option>2014</option>
                            <option>2015</option>
                            <option>2016</option>
                            <option>2017</option>
                            <option>2018</option>
                            <option>2019</option>
                            <option>2020</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Gender</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" id="femaleRadio" name="gender" required value="female">Female
                                </label>
                            </div>
                            <div class="col-sm-4">
                                <label class="radio-inline">
                                    <input type="radio" id="maleRadio" name="gender" required value="male">Male
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Extra Curricular Interests</label>
                    <div class="col-sm-9" id="checkboxDiv">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="sportsCheckbox" value="sports">Sports
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="programmingCheckbox" value="programming">Programming
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="artsCheckbox" value="arts">Arts
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="musicCheckbox" value="music">Music
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 col-md-3">
                        <button type="submit" class="btn btn-primary btn-block">ADD</button>
                    </div>
                    <div class="col-sm-9 col-sm-offset-3 col-md-3">
                        <button id="reset" type="reset" class="btn btn-primary btn-block">reset</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
<!doctype html>
<html>
    <head>
        <title>SMS</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
    </head>
</html>
    <body>
        <header role="banner" class="navbar navbar-fixed-top navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <button data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" type="button" class="navbar-toggle pull-left"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <div class="navbar-inverse side-collapse in">
                    <nav role="navigation" class="navbar-collapse">
                        <ul class="nav navbar-nav">
                        <li><a href="/sms">Home</a></li>
                        <li><a href="/sms/index.php/add">Add New Student</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <div class="container" style="width:75%;margin-top:2%">
            <table class="table">
            <tr>
                <td><strong> Name</strong></td>
                <td><strong> Email</strong></td>
                <td><strong> Gender</strong></td>
                <td><strong> YOP</strong></td>
                <td><strong>Edit</strong></td>
            </tr>
            <?php foreach ($data as $value): ?>
            <tr>
                <td><?php echo $value['name']; ?></td>
                <td><?php echo $value['email']; ?></td>
                <td><?php echo $value['gender']; ?></td>
                <td><?php echo $value['yop']; ?></td>
                <td>
                    <a href="<?php echo site_url('index.php/student/edit/'.$value['email']); ?>">Edit</a> | 
                    <a href="<?php echo site_url('index.php/student/delete/'.$value['email']); ?>" onClick="return confirm('Are you sure you want to delete?')">Delete</a>
                </td>
            </tr>
            <?php  endforeach; ?>
        </table>
        <p style="text-align:center"><?php if(!count($data)) echo "no results found"; ?></p>
        </div>
    </body>
    <script src="<?php echo base_url("assets/js/app.js");?>"></script>          
</html>